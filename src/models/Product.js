const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const ProductSchema = new mongoose.Schema({
  
    id: String,
    title: [String],
    img: String,
    status: String,
    price: Number ,
    sPrice: Number,
    
}, {
    toJSON: {
      virtuals: true,
    } 
});

ProductSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Product', ProductSchema);