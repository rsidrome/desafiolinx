const Product = require('../models/Product');

module.exports = {
    async show(req, res){

          const { page = 1} = req.query; 
          const product = await Product.paginate({}, {page, limit: 10} );
         
          return res.json(product);
    
    },
};