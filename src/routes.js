const express = require('express');
const routes = express.Router();
const ProductController = require('./controllers/ProductController');

//Rotas
routes.get("/search", ProductController.show) ;


module.exports = routes;