const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const requireDir = require('require-dir');


//crio o app
const app = express();
app.use(express.json());
app.use(cors());

//iniciando o BD

mongoose.connect('mongodb+srv://rsidrome:sidrome2@cluster0-d9jzg.mongodb.net/test?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
})

requireDir("./src/models");

app.use('/', require('./src/routes'));

app.listen(9000);